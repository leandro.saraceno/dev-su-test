package com.devsu.test.account.service;

import com.devsu.test.account.dto.AccountRequestDTO;
import com.devsu.test.account.dto.AccountResponseDTO;
import com.devsu.test.account.dto.AccountUpdateRequestDTO;

import java.util.List;

public interface AccountService {

    AccountResponseDTO create(AccountRequestDTO dto);

    AccountResponseDTO update(AccountUpdateRequestDTO dto);

    void delete(Integer id);

    List<AccountResponseDTO> getAll();

    AccountResponseDTO getById(Integer id);
}
