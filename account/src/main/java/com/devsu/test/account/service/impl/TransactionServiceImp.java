package com.devsu.test.account.service.impl;

import com.devsu.test.account.dto.TransactionRequestDTO;
import com.devsu.test.account.dto.TransactionResponseDTO;
import com.devsu.test.account.dto.TransactionUpdateRequestDTO;
import com.devsu.test.account.exception.ConflictException;
import com.devsu.test.account.exception.NotFoundException;
import com.devsu.test.account.model.Transaction;
import com.devsu.test.account.model.TransactionType;
import com.devsu.test.account.repository.TransactionRepository;
import com.devsu.test.account.repository.TransactionTypeRepository;
import com.devsu.test.account.service.AccountService;
import com.devsu.test.account.service.TransactionService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Transactional
@AllArgsConstructor
@Service
public class TransactionServiceImp implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final TransactionTypeRepository transactionTypeRepository;
    private final AccountService accountService;
    private final ConversionService conversionService;

    public TransactionResponseDTO create(TransactionRequestDTO dto) {
        Transaction newTransaction = conversionService.convert(dto, Transaction.class);
        return save(newTransaction);
    }

    public TransactionResponseDTO update(TransactionUpdateRequestDTO dto) {
        this.findById(dto.getId());
        Transaction newTransaction = conversionService.convert(dto, Transaction.class);
        return save(newTransaction);
    }

    public void delete(Integer id) {
        this.findById(id);
        this.transactionRepository.deleteById(id);
    }


    public List<TransactionResponseDTO> getAll() {
        return this.transactionRepository.findAll().stream().map(c -> conversionService.convert(c, TransactionResponseDTO.class)).toList();
    }

    public TransactionResponseDTO getById(Integer id) {
        return conversionService.convert(this.findById(id), TransactionResponseDTO.class);
    }

    private TransactionResponseDTO save(Transaction newTransaction) {

        TransactionType type = transactionTypeRepository.findById(newTransaction.getType().getId())
                .orElseThrow(() -> new NotFoundException("Transaction type not found"));

        // Get the current balance
        double currentBalance = transactionRepository.findFirstByAccount_IdOrderByDateDesc(newTransaction.getAccount().getId())
                .map(t -> t.getBalance() + newTransaction.getValue())
                .orElseGet(() -> {
                    var initialBalance = accountService.getById(newTransaction.getAccount().getId()).getInitialBalance();
                    return initialBalance + newTransaction.getValue();
                });
        if (currentBalance < 0.d) {
            throw new ConflictException("Saldo no disponible");
        }
        // Set balance and type
        newTransaction.setBalance(currentBalance);
        newTransaction.setType(type);

        return conversionService.convert(this.transactionRepository.save(Objects.requireNonNull(newTransaction)), TransactionResponseDTO.class);
    }

    private Transaction findById(Integer id) {
        return this.transactionRepository.findById(id).orElseThrow(() -> new NotFoundException("transaction not found"));
    }

}
