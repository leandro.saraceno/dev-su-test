package com.devsu.test.account.service;

import com.devsu.test.account.dto.AccountReportDTO;

import java.time.LocalDate;

public interface ReportService {

    AccountReportDTO getAccountReport(Integer clientId, LocalDate startDate, LocalDate endDate);

}
