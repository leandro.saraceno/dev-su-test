package com.devsu.test.account.exception;

import org.springframework.http.HttpStatus;

public class InternalErrorException extends GenericException {

	private static final long serialVersionUID = 1L;

	public InternalErrorException(String message) {
		super(HttpStatus.INTERNAL_SERVER_ERROR, message);
	}

}
