package com.devsu.test.account.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequestDTO {

    @NotNull
    private LocalDate date;
    @NotNull
    private Integer typeId;
    private double value;
    private double balance;
    @NotNull
    private Integer accountId;

}
