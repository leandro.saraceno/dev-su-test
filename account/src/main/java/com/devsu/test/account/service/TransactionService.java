package com.devsu.test.account.service;

import com.devsu.test.account.dto.TransactionRequestDTO;
import com.devsu.test.account.dto.TransactionResponseDTO;
import com.devsu.test.account.dto.TransactionUpdateRequestDTO;

import java.util.List;

public interface TransactionService {

    TransactionResponseDTO create(TransactionRequestDTO dto);

    TransactionResponseDTO update(TransactionUpdateRequestDTO dto);

    void delete(Integer id);

    List<TransactionResponseDTO> getAll();

    TransactionResponseDTO getById(Integer id);
}
