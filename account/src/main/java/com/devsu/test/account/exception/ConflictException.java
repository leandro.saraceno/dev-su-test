package com.devsu.test.account.exception;

import org.springframework.http.HttpStatus;

public class ConflictException extends GenericException{
    public ConflictException(String message) {
        super(HttpStatus.CONFLICT, message);
    }
}
