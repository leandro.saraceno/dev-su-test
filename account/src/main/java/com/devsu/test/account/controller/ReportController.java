package com.devsu.test.account.controller;

import com.devsu.test.account.dto.AccountReportDTO;
import com.devsu.test.account.service.ReportService;
import com.devsu.test.account.service.impl.ReportServiceImp;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@AllArgsConstructor
@RestController
@RequestMapping("/reportes")
public class ReportController {

    private final ReportService reportService;

    @Operation(summary = "View a list of available accounts")
    @GetMapping
    public ResponseEntity<AccountReportDTO> getAccountReport(
            @RequestParam(name = "client") Integer clientId,
            @RequestParam(name = "start") LocalDate startDate,
            @RequestParam(name = "end") LocalDate endDate) {
        return ResponseEntity.ok(this.reportService.getAccountReport(clientId, startDate, endDate));
    }
}
