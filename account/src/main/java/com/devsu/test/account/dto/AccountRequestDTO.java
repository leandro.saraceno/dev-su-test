package com.devsu.test.account.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AccountRequestDTO {

    @NotNull
    private Integer typeId;
    private double initialBalance;
    private boolean status;
    @NotNull
    private Integer clientId;

}
