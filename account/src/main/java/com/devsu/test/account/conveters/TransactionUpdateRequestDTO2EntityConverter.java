package com.devsu.test.account.conveters;

import com.devsu.test.account.dto.TransactionRequestDTO;
import com.devsu.test.account.dto.TransactionUpdateRequestDTO;
import com.devsu.test.account.model.Account;
import com.devsu.test.account.model.Transaction;
import com.devsu.test.account.model.TransactionType;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TransactionUpdateRequestDTO2EntityConverter implements Converter<TransactionUpdateRequestDTO, Transaction> {

    @Override
    public Transaction convert(TransactionUpdateRequestDTO source) {
        Transaction transaction = new Transaction();
        transaction.setId(source.getId());
        Account account = new Account();
        account.setId(source.getAccountId());
        transaction.setAccount(account);
        TransactionType transactionType = new TransactionType();
        transactionType.setId(source.getTypeId());
        transaction.setType(transactionType);
        transaction.setBalance(source.getBalance());
        transaction.setValue(source.getValue());
        transaction.setDate(source.getDate());
        return transaction;
    }
}
