package com.devsu.test.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AccountResponseDTO {

    private Integer id;
    private String type;
    private double initialBalance;
    private boolean status;
    private Integer clientId;

}
