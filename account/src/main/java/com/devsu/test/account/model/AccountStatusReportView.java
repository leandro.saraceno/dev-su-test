package com.devsu.test.account.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "account_status_report_view")
public class AccountStatusReportView {

    @Id
    private Integer id;
    private Integer clientId;
    private Integer accountId;
    private String type;
    private double currentBalance;

}
