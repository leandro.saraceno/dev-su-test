package com.devsu.test.account.controller;

import com.devsu.test.account.dto.AccountRequestDTO;
import com.devsu.test.account.dto.AccountResponseDTO;
import com.devsu.test.account.dto.AccountUpdateRequestDTO;
import com.devsu.test.account.service.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for managing accounts.
 */
@RestController
@RequestMapping("/cuentas")
@Tag(name = "Account", description = "Operations pertaining to accounts in the Account Management System")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    /**
     * Create a new account.
     *
     * @param dto the account to create
     * @return the created account
     */
    @Operation(summary = "Create a new account")
    @PostMapping
    public ResponseEntity<AccountResponseDTO> createAccount(@Valid @RequestBody AccountRequestDTO dto) {
        return new ResponseEntity<>(this.accountService.create(dto), HttpStatus.CREATED);
    }

    /**
     * Update an existing account.
     *
     * @param dto the account to update
     * @return the updated account
     */
    @Operation(summary = "Update an existing account")
    @PutMapping
    public ResponseEntity<AccountResponseDTO> updateAccount(@Valid @RequestBody AccountUpdateRequestDTO dto) {
        return ResponseEntity.ok(this.accountService.update(dto));
    }

    /**
     * Delete an existing account.
     *
     * @param id the id of the account to delete
     */
    @Operation(summary = "Update an existing account")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAccount(@PathVariable Integer id) {
        this.accountService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * Get all accounts.
     *
     * @return list of all accounts
     */
    @Operation(summary = "View a list of available accounts")
    @GetMapping
    public ResponseEntity<List<AccountResponseDTO>> getAllAccounts() {
        return ResponseEntity.ok(this.accountService.getAll());
    }

    /**
     * Get an account by id.
     *
     * @param id the id of the account to retrieve
     * @return the account with the specified id
     */
    @Operation(summary = "Get a account by Id")
    @GetMapping("/{id}")
    public ResponseEntity<AccountResponseDTO> getAccountById(@PathVariable Integer id) {
        return ResponseEntity.ok(this.accountService.getById(id));
    }
}
