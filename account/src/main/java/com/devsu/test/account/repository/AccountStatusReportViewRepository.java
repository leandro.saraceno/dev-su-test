package com.devsu.test.account.repository;

import com.devsu.test.account.model.AccountStatusReportView;

import java.util.List;

public interface AccountStatusReportViewRepository extends ViewRepository<AccountStatusReportView, Integer> {

    List<AccountStatusReportView> findALlByClientId(Integer clientId);
}
