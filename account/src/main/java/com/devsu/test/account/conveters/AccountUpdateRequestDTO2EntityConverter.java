package com.devsu.test.account.conveters;

import com.devsu.test.account.dto.AccountRequestDTO;
import com.devsu.test.account.dto.AccountUpdateRequestDTO;
import com.devsu.test.account.model.Account;
import com.devsu.test.account.model.AccountType;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AccountUpdateRequestDTO2EntityConverter implements Converter<AccountUpdateRequestDTO, Account> {


    @Override
    public Account convert(AccountUpdateRequestDTO source) {
        Account account = new Account();
        account.setId(source.getId());
        account.setStatus(source.isStatus());
        account.setInitialBalance(source.getInitialBalance());
        account.setClientId(source.getClientId());

        AccountType accountType = new AccountType();
        accountType.setId(source.getTypeId());
        account.setType(accountType);

        return account;
    }
}
