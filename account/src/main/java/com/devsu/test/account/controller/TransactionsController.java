package com.devsu.test.account.controller;

import com.devsu.test.account.dto.TransactionRequestDTO;
import com.devsu.test.account.dto.TransactionResponseDTO;
import com.devsu.test.account.dto.TransactionUpdateRequestDTO;
import com.devsu.test.account.service.TransactionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for managing transactions.
 */
@RestController
@RequestMapping("/movimientos")
@Tag(name = "Transaction", description = "Operations related to transactions in the Transaction Management System")
public class TransactionsController {

    private final TransactionService transactionService;

    public TransactionsController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    /**
     * Create a new transaction.
     *
     * @param dto the transaction to create
     * @return the created transaction
     */
    @Operation(summary = "Create a new transaction")
    @PostMapping
    public ResponseEntity<TransactionResponseDTO> createTransaction(@Valid @RequestBody TransactionRequestDTO dto) {
        return new ResponseEntity<>(this.transactionService.create(dto), HttpStatus.CREATED);
    }

    /**
     * Update an existing transaction.
     *
     * @param dto the transaction to update
     * @return the updated transaction
     */
    @Operation(summary = "Update an existing transaction")
    @PutMapping
    public ResponseEntity<TransactionResponseDTO> updateTransaction(@Valid  @RequestBody TransactionUpdateRequestDTO dto) {
        return ResponseEntity.ok(this.transactionService.update(dto));
    }

    /**
     * Delete an existing transaction.
     *
     * @param id the id of the transaction to delete
     */
    @Operation(summary = "Update an existing transaction")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTransaction(@PathVariable Integer id) {
        this.transactionService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * Get all transactions.
     *
     * @return list of all transactions
     */
    @Operation(summary = "View a list of available transactions")
    @GetMapping
    public ResponseEntity<List<TransactionResponseDTO>> getAllTransactions() {
        return ResponseEntity.ok(this.transactionService.getAll());
    }

    /**
     * Get an transaction by id.
     *
     * @param id the id of the transaction to retrieve
     * @return the transaction with the specified id
     */
    @Operation(summary = "Get a transaction by Id")
    @GetMapping("/{id}")
    public ResponseEntity<TransactionResponseDTO> getTransactionById(@PathVariable Integer id) {
        return ResponseEntity.ok(this.transactionService.getById(id));
    }
}

