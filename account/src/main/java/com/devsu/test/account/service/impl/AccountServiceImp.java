package com.devsu.test.account.service.impl;

import com.devsu.test.account.dto.AccountRequestDTO;
import com.devsu.test.account.dto.AccountResponseDTO;
import com.devsu.test.account.dto.AccountUpdateRequestDTO;
import com.devsu.test.account.exception.NotFoundException;
import com.devsu.test.account.model.Account;
import com.devsu.test.account.model.AccountType;
import com.devsu.test.account.repository.AccountRepository;
import com.devsu.test.account.repository.AccountTypeRepository;
import com.devsu.test.account.service.AccountService;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Transactional
@Service
public class AccountServiceImp implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountTypeRepository accountTypeRepository;
    private final ConversionService conversionService;


    public AccountServiceImp(AccountRepository accountRepository, ConversionService conversionService, AccountTypeRepository accountTypeRepository) {
        this.accountRepository = accountRepository;
        this.conversionService = conversionService;
        this.accountTypeRepository = accountTypeRepository;
    }

    public AccountResponseDTO create(AccountRequestDTO dto) {
        return save(dto);
    }

    public AccountResponseDTO update(AccountUpdateRequestDTO dto) {
        this.findById(dto.getId());
        return save(dto);
    }

    public void delete(Integer id) {
        this.findById(id);
        this.accountRepository.deleteById(id);
    }


    public List<AccountResponseDTO> getAll() {
        return this.accountRepository.findAll().stream().map(c -> conversionService.convert(c, AccountResponseDTO.class)).toList();
    }

    public AccountResponseDTO getById(Integer id) {
        return conversionService.convert(this.findById(id), AccountResponseDTO.class);
    }

    private AccountResponseDTO save(AccountRequestDTO dto) {
        Account newAccount = conversionService.convert(dto, Account.class);
        AccountType type = accountTypeRepository.findById(dto.getTypeId())
                .orElseThrow(() -> new NotFoundException("Account type not found"));

        // Set the AccountType
        newAccount.setType(type);

        return conversionService.convert(this.accountRepository.save(Objects.requireNonNull(newAccount)), AccountResponseDTO.class);
    }

    private Account findById(Integer id) {
        return this.accountRepository.findById(id).orElseThrow(() -> new NotFoundException("account not found"));
    }

}
