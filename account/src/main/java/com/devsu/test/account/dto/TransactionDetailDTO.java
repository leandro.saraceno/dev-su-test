package com.devsu.test.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDetailDTO {

    private Long id;
    private LocalDate date;
    private String type;
    private Double value;
    private Double balance;
}
