package com.devsu.test.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class AccountStatusReportDTO {

    private Integer id;
    private Integer clientId;
    private Integer accountId;
    private String type;
    private double currentBalance;
    private List<TransactionResponseDTO> transactionDetails;
}
