package com.devsu.test.account.conveters;

import com.devsu.test.account.dto.AccountResponseDTO;
import com.devsu.test.account.model.Account;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class Account2AccountResponseDTOConverter implements Converter<Account, AccountResponseDTO> {

    private final ModelMapper modelMapper;

    public Account2AccountResponseDTOConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AccountResponseDTO convert(Account source) {
        AccountResponseDTO dto = this.modelMapper.map(source, AccountResponseDTO.class);
        dto.setType(source.getType().getName());
        return dto;
    }
}
