package com.devsu.test.account.service.impl;

import com.devsu.test.account.dto.AccountReportDTO;
import com.devsu.test.account.dto.AccountStatusReportDTO;
import com.devsu.test.account.dto.TransactionResponseDTO;
import com.devsu.test.account.repository.AccountStatusReportViewRepository;
import com.devsu.test.account.repository.TransactionRepository;
import com.devsu.test.account.service.ReportService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Service
public class ReportServiceImp implements ReportService {

    private final AccountStatusReportViewRepository accountStatusReportViewRepository;
    private final TransactionRepository transactionRepository;
    private final ConversionService conversionService;

    public AccountReportDTO getAccountReport(Integer clientId, LocalDate startDate, LocalDate endDate) {

        List<AccountStatusReportDTO> accountList = this.accountStatusReportViewRepository.findALlByClientId(clientId)
                .stream().map(account -> conversionService.convert(account, AccountStatusReportDTO.class)).toList();

        accountList.forEach(accountStatusReportDTO -> accountStatusReportDTO.setTransactionDetails(
                transactionRepository.findAllByAccount_IdAndDateBetween(accountStatusReportDTO.getAccountId(), startDate, endDate)
                        .stream().map(transaction ->
                                conversionService.convert(transaction, TransactionResponseDTO.class))
                        .toList()

        ));

        return new AccountReportDTO(accountList);
    }
}
