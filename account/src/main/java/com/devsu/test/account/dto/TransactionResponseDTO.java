package com.devsu.test.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionResponseDTO {

    private Integer id;
    private LocalDate date;
    private String type;
    private double value;
    private double balance;
    private Integer accountId;

}
