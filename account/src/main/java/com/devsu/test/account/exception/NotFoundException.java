package com.devsu.test.account.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends GenericException {

    private static final long serialVersionUID = 1L;

    public NotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
