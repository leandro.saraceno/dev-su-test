package com.devsu.test.account.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class AccountReportDTO {

    private List<AccountStatusReportDTO> account;
}
