package com.devsu.test.account.conveters;

import com.devsu.test.account.dto.AccountRequestDTO;
import com.devsu.test.account.dto.AccountResponseDTO;
import com.devsu.test.account.model.Account;
import com.devsu.test.account.model.AccountType;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AccountRequestDTO2EntityConverter implements Converter<AccountRequestDTO, Account> {


    @Override
    public Account convert(AccountRequestDTO source) {

        Account account = new Account();
        account.setStatus(source.isStatus());
        account.setInitialBalance(source.getInitialBalance());
        account.setClientId(source.getClientId());

        AccountType accountType = new AccountType();
        accountType.setId(source.getTypeId());
        account.setType(accountType);

        return account;
    }
}
