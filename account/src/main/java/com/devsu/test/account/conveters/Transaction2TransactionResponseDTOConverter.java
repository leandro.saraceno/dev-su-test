package com.devsu.test.account.conveters;

import com.devsu.test.account.dto.TransactionResponseDTO;
import com.devsu.test.account.model.Transaction;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class Transaction2TransactionResponseDTOConverter implements Converter<Transaction, TransactionResponseDTO> {

    private final ModelMapper modelMapper;

    public Transaction2TransactionResponseDTOConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public TransactionResponseDTO convert(Transaction source) {
        TransactionResponseDTO dto = this.modelMapper.map(source, TransactionResponseDTO.class);
        dto.setType(source.getType().getName());
        dto.setAccountId(source.getAccount().getId());
        return dto;
    }
}
