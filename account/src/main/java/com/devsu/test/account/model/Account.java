package com.devsu.test.account.model;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "accounts")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private double initialBalance;
    private Integer clientId;
    private boolean status;

    @ManyToOne
    @JoinColumn(name = "type_id", nullable = false)
    private AccountType type;

}
