package com.devsu.test.account.dto;

import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

public class ErrorResponseDTO {

    private int code;
    private List<String> messages;
    private String status;

    public ErrorResponseDTO() {
        super();
    }

    public ErrorResponseDTO(HttpStatus code, String message) {
        this.code = code.value();
        this.status = code.getReasonPhrase();
        this.messages = Collections.singletonList(message);
    }

    public ErrorResponseDTO(HttpStatus code, List<String> messages) {
        this.code = code.value();
        this.status = code.getReasonPhrase();
        this.messages = messages;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
