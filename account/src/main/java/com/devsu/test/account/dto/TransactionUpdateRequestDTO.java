package com.devsu.test.account.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class TransactionUpdateRequestDTO extends TransactionRequestDTO {

    @NotNull
    private Integer id;

}
