package com.devsu.test.account.conveters;

import com.devsu.test.account.dto.AccountResponseDTO;
import com.devsu.test.account.dto.AccountStatusReportDTO;
import com.devsu.test.account.model.Account;
import com.devsu.test.account.model.AccountStatusReportView;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AccountStatusReportView2AccountStatusReportDTOConverter implements Converter<AccountStatusReportView, AccountStatusReportDTO> {

    private final ModelMapper modelMapper;

    public AccountStatusReportView2AccountStatusReportDTOConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AccountStatusReportDTO convert(AccountStatusReportView source) {
        AccountStatusReportDTO dto = this.modelMapper.map(source, AccountStatusReportDTO.class);
        return dto;
    }
}
