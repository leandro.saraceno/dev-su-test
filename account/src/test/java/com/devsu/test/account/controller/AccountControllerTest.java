package com.devsu.test.account.controller;


import com.devsu.test.account.dto.AccountRequestDTO;
import com.devsu.test.account.dto.AccountUpdateRequestDTO;
import com.devsu.test.account.mocks.AccountMock;
import com.devsu.test.account.service.AccountService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountControllerTest {

    @InjectMocks
    private AccountController accountController;

    @Mock
    private AccountService accountService;


    @Test
    void createAccount_success() {
        when(accountService.create(any(AccountRequestDTO.class))).thenReturn(AccountMock.accountResponseDTO());
        var response = accountController.createAccount(AccountMock.accountRequestDTO());

        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.getId());
    }


    @Test
    void updateAccount_success() {
        when(accountService.update(any(AccountUpdateRequestDTO.class))).thenReturn(AccountMock.accountResponseDTO());
        var response = accountController.updateAccount(AccountMock.accountUpdateRequestDTO());

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.getId());
    }


    @Test
    void deleteAccount_success() {
        doNothing().when(accountService).delete(anyInt());
        var response = accountController.deleteAccount(1);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNull(response.getBody());
    }


    @Test
    void getAllAccounts_success() {
        when(accountService.getAll()).thenReturn(AccountMock.getAccountList());
        var response = accountController.getAllAccounts();

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(2, body.size());
        assertEquals(1, body.get(0).getId());
        assertEquals(2, body.get(1).getId());

    }


    @Test
    void getAccountById_success() {
        when(accountService.getById(anyInt())).thenReturn(AccountMock.accountResponseDTO());
        var response = accountController.getAccountById(1);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.getId());
    }

}
