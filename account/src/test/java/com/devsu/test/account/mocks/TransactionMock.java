package com.devsu.test.account.mocks;

import com.devsu.test.account.dto.TransactionRequestDTO;
import com.devsu.test.account.dto.TransactionResponseDTO;
import com.devsu.test.account.dto.TransactionUpdateRequestDTO;

import java.time.LocalDate;
import java.util.List;

public class TransactionMock {

    public static TransactionRequestDTO transactionRequestDTO() {
        TransactionRequestDTO dto = new TransactionRequestDTO();
        dto.setDate(LocalDate.of(2024, 1, 20));
        dto.setValue(100);
        dto.setTypeId(1);
        dto.setAccountId(1);
        return dto;
    }


    public static TransactionResponseDTO transactionResponseDTO() {
        TransactionResponseDTO dto = new TransactionResponseDTO();
        dto.setId(1);
        dto.setDate(LocalDate.of(2024, 1, 20));
        dto.setValue(100);
        dto.setType("DEB");
        dto.setBalance(100);
        return dto;
    }

    public static TransactionUpdateRequestDTO transactionUpdateRequestDTO() {
        TransactionUpdateRequestDTO dto = new TransactionUpdateRequestDTO();
        dto.setId(1);
        dto.setTypeId(2);
        dto.setDate(LocalDate.of(2024, 1, 20));
        dto.setValue(-100);
        return dto;
    }


    public static List<TransactionResponseDTO> getTransactionList() {

        TransactionResponseDTO dto1 = new TransactionResponseDTO();
        dto1.setId(1);
        dto1.setType("DEB");
        dto1.setDate(LocalDate.of(2024, 1, 20));
        dto1.setValue(100);
        dto1.setBalance(100);

        TransactionResponseDTO dto2 = new TransactionResponseDTO();
        dto2.setId(2);
        dto2.setType("CRED");
        dto1.setDate(LocalDate.of(2024, 1, 25));
        dto1.setValue(-50);
        dto1.setBalance(50);

        return List.of(dto1, dto2);
    }
}
