package com.devsu.test.account.mocks;

import com.devsu.test.account.dto.AccountReportDTO;
import com.devsu.test.account.dto.AccountStatusReportDTO;
import com.devsu.test.account.dto.TransactionResponseDTO;

import java.time.LocalDate;
import java.util.List;

public class ReportMock {

    public static AccountReportDTO accountReportDTO() {
        AccountStatusReportDTO dto = new AccountStatusReportDTO();
        dto.setAccountId(1);
        dto.setType("Corriente");
        dto.setClientId(1);
        dto.setCurrentBalance(1000);

        TransactionResponseDTO d1 = new TransactionResponseDTO();
        d1.setValue(500d);
        d1.setBalance(500d);
        d1.setType("DEB");
        d1.setDate(LocalDate.of(2024, 1, 25));

        TransactionResponseDTO d2 = new TransactionResponseDTO();
        d2.setValue(500d);
        d2.setBalance(1000d);
        d2.setType("DEB");
        d2.setDate(LocalDate.of(2024, 1, 26));


        dto.setTransactionDetails(List.of(d1, d2));

        return new AccountReportDTO(List.of(dto));
    }
}
