package com.devsu.test.account.mocks;

import com.devsu.test.account.dto.AccountRequestDTO;
import com.devsu.test.account.dto.AccountResponseDTO;
import com.devsu.test.account.dto.AccountUpdateRequestDTO;

import java.util.List;

public class AccountMock {

    public static AccountRequestDTO accountRequestDTO() {
        AccountRequestDTO dto = new AccountRequestDTO();
        dto.setTypeId(1);
        dto.setInitialBalance(0.d);
        dto.setStatus(true);
        dto.setClientId(1);
        return dto;
    }

    public static AccountResponseDTO accountResponseDTO() {
        AccountResponseDTO dto = new AccountResponseDTO();
        dto.setId(1);
        dto.setType("Ahorros");
        dto.setInitialBalance(0.d);
        dto.setStatus(true);
        dto.setClientId(1);
        return dto;
    }

    public static AccountUpdateRequestDTO accountUpdateRequestDTO() {
        AccountUpdateRequestDTO dto = new AccountUpdateRequestDTO();
        dto.setTypeId(1);
        dto.setInitialBalance(0.d);
        dto.setStatus(true);
        dto.setClientId(1);
        return dto;
    }


    public static List<AccountResponseDTO> getAccountList() {

        AccountResponseDTO dto1 = new AccountResponseDTO();
        dto1.setId(1);
        dto1.setType("Ahorros");
        dto1.setInitialBalance(0.d);
        dto1.setStatus(true);
        dto1.setClientId(1);

        AccountResponseDTO dto2 = new AccountResponseDTO();
        dto2.setId(2);
        dto2.setType("Corriente");
        dto2.setInitialBalance(0.d);
        dto2.setStatus(true);
        dto2.setClientId(1);
        return List.of(dto1,dto2);
    }
}
