package com.devsu.test.account.controller;


import com.devsu.test.account.dto.TransactionRequestDTO;
import com.devsu.test.account.dto.TransactionUpdateRequestDTO;
import com.devsu.test.account.mocks.TransactionMock;
import com.devsu.test.account.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransactionControllerTest {

    @InjectMocks
    private TransactionsController transactionController;

    @Mock
    private TransactionService transactionService;


    @Test
    void createTransaction_success() {
        when(transactionService.create(any(TransactionRequestDTO.class))).thenReturn(TransactionMock.transactionResponseDTO());
        var response = transactionController.createTransaction(TransactionMock.transactionRequestDTO());

        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.getId());
    }


    @Test
    void updateTransaction_success() {
        when(transactionService.update(any(TransactionUpdateRequestDTO.class))).thenReturn(TransactionMock.transactionResponseDTO());
        var response = transactionController.updateTransaction(TransactionMock.transactionUpdateRequestDTO());

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.getId());
    }


    @Test
    void deleteTransaction_success() {
        doNothing().when(transactionService).delete(anyInt());
        var response = transactionController.deleteTransaction(1);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNull(response.getBody());
    }


    @Test
    void getAllTransactions_success() {
        when(transactionService.getAll()).thenReturn(TransactionMock.getTransactionList());
        var response = transactionController.getAllTransactions();

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(2, body.size());
        assertEquals(1, body.get(0).getId());
        assertEquals(2, body.get(1).getId());

    }


    @Test
    void getTransactionById_success() {
        when(transactionService.getById(anyInt())).thenReturn(TransactionMock.transactionResponseDTO());
        var response = transactionController.getTransactionById(1);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.getId());
    }
}
