package com.devsu.test.account.controller;


import com.devsu.test.account.mocks.ReportMock;
import com.devsu.test.account.service.ReportService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReportControllerTest {

    @InjectMocks
    private ReportController reportController;

    @Mock
    private ReportService reportService;


    @Test
    void getAccountReport_success() {
        var start = LocalDate.of(2024, 1, 20);
        var end = LocalDate.of(2024, 1, 27);

        when(reportService.getAccountReport(anyInt(), any(LocalDate.class), any(LocalDate.class)))
                .thenReturn(ReportMock.accountReportDTO());


        var response = reportController.getAccountReport(1, start, end);


        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

}
