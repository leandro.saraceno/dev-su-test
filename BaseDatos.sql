-- Create table for People
CREATE TABLE PEOPLE (
                        id SERIAL PRIMARY KEY,
                        "name" VARCHAR(255) NOT NULL,
                        gender VARCHAR(10) NOT NULL,
                        birth_date DATE NOT NULL,
                        tel VARCHAR(20),
                        address VARCHAR(255)
);

-- Create table for Client
CREATE TABLE clients (
                         id INTEGER PRIMARY KEY,
                         client_id VARCHAR(50) NOT NULL,
                         password VARCHAR(255) NOT NULL,
                         status BOOLEAN NOT NULL,
                         CONSTRAINT fk_clients_people FOREIGN KEY (id) REFERENCES people (id)
);


-- Create table for account_types
CREATE TABLE account_types (
                               id SERIAL PRIMARY KEY,
                               "name" VARCHAR(255) NOT NULL
);

-- Create table for accounts
CREATE TABLE accounts (
                          id SERIAL PRIMARY KEY,
                          initial_balance DOUBLE PRECISION NOT NULL,
                          client_id INTEGER NOT NULL,
                          status BOOLEAN NOT NULL,
                          type_id INTEGER NOT NULL,
                          CONSTRAINT fk_account_type FOREIGN KEY (type_id) REFERENCES account_types (id),
                          CONSTRAINT fk_clients FOREIGN KEY (client_id) REFERENCES clients (id)
);

-- Create table for transaction_types
CREATE TABLE transaction_types (
                                   id SERIAL PRIMARY KEY,
                                   "name" VARCHAR(255) NOT NULL
);

-- Create table for transactions
CREATE TABLE transactions (
                              id SERIAL PRIMARY KEY,
                              "date" DATE NOT NULL,
                              type_id INTEGER NOT NULL,
                              value DOUBLE PRECISION NOT NULL,
                              balance DOUBLE PRECISION NOT NULL,
                              account_id INTEGER NOT NULL,
                              CONSTRAINT fk_transaction_type FOREIGN KEY (type_id) REFERENCES transaction_types (id),
                              CONSTRAINT fk_transaction_acccount FOREIGN KEY (account_id) REFERENCES accounts (id)
);


CREATE OR REPLACE VIEW public.account_status_report_view
AS SELECT row_number() OVER () AS id,
          c.id AS client_id,
          a.id AS account_id,
          act.name AS type,
          COALESCE(sum(t.value), 0::double precision) AS current_balance
   FROM accounts a
            JOIN clients c ON c.id = a.client_id
            JOIN account_types act ON act.id = a.type_id
            LEFT JOIN transactions t ON a.id = t.account_id
   GROUP BY a.id, c.id, act.name;


   -- Insert into people table
INSERT INTO people (name, gender, birth_date, tel, address) VALUES
('Jose Lema', 'Male', '1980-01-01', '098254785', 'Otavalo sn y principal'),
('Marianela Montalvo', 'Female', '1985-05-15', '097548965', 'Amazonas y NNUU'),
('Juan Osorio', 'Male', '1990-09-20', '098874587', '13 junio y Equinoccial');

-- Insert into client table
INSERT INTO clients (id, client_id, password, status) VALUES
((SELECT id FROM people WHERE "name" = 'Jose Lema'), 'jose.lema', '1234', TRUE),
((SELECT id FROM people WHERE "name" = 'Marianela Montalvo'), 'marianela.montalvo', '5678', TRUE),
((SELECT id FROM people WHERE "name" = 'Juan Osorio'), 'juan.osorio', '1245', TRUE);

-- Insert into account_types table
INSERT INTO account_types (name) VALUES
('Ahorros'),
('Corriente');

-- Insert into accounts table
INSERT INTO accounts (id, initial_balance, client_id, status, type_id) VALUES
(478758, 2000, (SELECT id FROM people WHERE "name" = 'Jose Lema'), TRUE, (SELECT id FROM account_types WHERE name = 'Ahorros')),
(225487, 100, (SELECT id FROM people WHERE "name" = 'Marianela Montalvo'), TRUE, (SELECT id FROM account_types WHERE name = 'Corriente')),
(495878, 0, (SELECT id FROM people WHERE "name" = 'Juan Osorio'), TRUE, (SELECT id FROM account_types WHERE name = 'Ahorros')),
(496825, 540, (SELECT id FROM people WHERE "name" = 'Marianela Montalvo'), TRUE, (SELECT id FROM account_types WHERE name = 'Ahorros'));

INSERT INTO public.transaction_types
(id, "name")
VALUES(1, 'Deposito');
INSERT INTO public.transaction_types
(id, "name")
VALUES(2, 'Retiro');


INSERT INTO public.transactions
(id, "date", type_id, value, balance, account_id)
VALUES(1, 'now()', 2, 575.0, 1425.0, 478758);

INSERT INTO public.transactions
(id, "date", type_id, value, balance, account_id)
VALUES(2, 'now()', 1, 600.0, 700.0, 225487);

INSERT INTO public.transactions
(id, "date", type_id, value, balance, account_id)
VALUES(3, 'now()', 1, 150, 150.0, 495878);

INSERT INTO public.transactions
(id, "date", type_id, value, balance, account_id)
VALUES(4, 'now()', 2, -540.0, 0, 496825);