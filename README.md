# How to Run Services

This guide outlines the steps to build and run the client and account services using Docker and Docker Compose.

## Prerequisites
- Docker installed on your machine ([Docker Installation Guide](https://docs.docker.com/get-docker/))
- Docker Compose installed on your machine ([Docker Compose Installation Guide](https://docs.docker.com/compose/install/))

## Steps

1. **Build Client and Account Services:**

   ```bash
   cd /client
   mvn clean install
   docker build -t client-service .
   
   cd /account
   mvn clean install
   docker build -t account-service .

   docker-compose up --build

2. **Info:**
When the docker compose is runing the database scripts will be running using liquibase. 

3. **Swagger Client and Account Services:**
- Client Service ([client](http://localhost:8080/swagger-ui/index.html))
- Account Service ([account](http://localhost:8081/swagger-ui/index.html))[README.md](README.md)

