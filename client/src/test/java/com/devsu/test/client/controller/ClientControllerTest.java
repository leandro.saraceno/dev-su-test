package com.devsu.test.client.controller;

import com.devsu.test.client.dto.ClientDTO;
import com.devsu.test.client.dto.ClientUpdateDTO;
import com.devsu.test.client.mocks.ClientMock;
import com.devsu.test.client.service.ClientService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientControllerTest {

    @InjectMocks
    private ClientController clientController;
    @Mock
    private ClientService clientService;


    @Test
    void createClient_success() {
        when(clientService.create(any(ClientDTO.class))).thenReturn(ClientMock.clientResponseDTO());
        var response = clientController.createClient(ClientMock.clientDTO());

        assertNotNull(response);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.getId());
    }


    @Test
    void updateClient_success() {
        when(clientService.update(any(ClientUpdateDTO.class))).thenReturn(ClientMock.clientResponseDTO());
        var response = clientController.updateClient(ClientMock.clientUpdateDTO());

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.getId());
    }


    @Test
    void deleteClient_success() {
        doNothing().when(clientService).delete(anyInt());
        var response = clientController.deleteClient(1);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNull(response.getBody());
    }


    @Test
    void getAllClients_success() {
        when(clientService.getAll()).thenReturn(ClientMock.clientResponseDTOList());
        var response = clientController.getAllClients();

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(2, body.size());
        assertEquals(1, body.get(0).getId());
        assertEquals(2, body.get(1).getId());

    }


    @Test
    void getClientById_success() {
        when(clientService.getById(anyInt())).thenReturn(ClientMock.clientResponseDTO());
        var response = clientController.getClientById(1);

        assertNotNull(response);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        var body = response.getBody();
        assertNotNull(body);
        assertEquals(1, body.getId());
    }
}
