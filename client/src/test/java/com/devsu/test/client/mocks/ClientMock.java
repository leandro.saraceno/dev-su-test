package com.devsu.test.client.mocks;

import com.devsu.test.client.dto.ClientDTO;
import com.devsu.test.client.dto.ClientResponseDTO;
import com.devsu.test.client.dto.ClientUpdateDTO;
import com.devsu.test.client.model.Client;

import java.util.Date;
import java.util.List;

public class ClientMock {

    public static Client client() {
        Client entity = new Client();
        entity.setPassword("1234");
        entity.setName("test name");
        return entity;
    }

    public static ClientDTO clientDTO() {
        return new ClientDTO("test name", "male", new Date(), "1232123", "address 123", "", "1234", true);
    }

    public static ClientUpdateDTO clientUpdateDTO() {
        var dto = new ClientUpdateDTO();
        dto.setId(1);
        dto.setName("test name");
        dto.setPassword("1234");
        dto.setTel("1232123");
        dto.setAddress("address 123");
        return dto;
    }

    public static ClientResponseDTO clientResponseDTO() {
        return new ClientResponseDTO(1, "test name", "male", new Date(), "1232123", "address 123", "", "1234", true);
    }

    public static List<ClientResponseDTO> clientResponseDTOList() {
        var c1 = new ClientResponseDTO(1, "test name 1", "male", new Date(), "1232123", "address 123", "", "1234", true);
        var c2 = new ClientResponseDTO(2, "test name 2", "male", new Date(), "1232123", "address 12345", "", "1234", true);

        return List.of(c1, c2);
    }

    public static List<Client> clientList() {
        var c1 = new Client();
        c1.setId(1);
        c1.setName("test name 1");
        c1.setPassword("1234");
        c1.setTel("1232123");
        c1.setAddress("address 123");

        var c2 = new Client();
        c2.setId(2);
        c2.setName("test name 2");
        c2.setPassword("1234");
        c2.setTel("1232123");
        c2.setAddress("address 123");

        return List.of(c1, c2);
    }


}
