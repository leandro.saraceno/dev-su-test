package com.devsu.test.client.service;

import com.devsu.test.client.dto.ClientDTO;
import com.devsu.test.client.dto.ClientResponseDTO;
import com.devsu.test.client.exception.NotFoundException;
import com.devsu.test.client.mocks.ClientMock;
import com.devsu.test.client.model.Client;
import com.devsu.test.client.repository.ClientRepository;
import com.devsu.test.client.service.impl.ClientServiceImp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.convert.ConversionService;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class ClientServiceImplTest {

    @Mock
    private ClientRepository clientRepository;
    @Mock
    private ConversionService conversionService;

    @InjectMocks
    private ClientServiceImp clientService;

    @Test
    void create() {

        ClientDTO client = ClientMock.clientDTO();
        Client entity = ClientMock.client();

        when(clientRepository.save(any(Client.class))).thenReturn(entity);
        when(conversionService.convert(client, Client.class))
                .thenReturn(entity);
        when(conversionService.convert(entity, ClientResponseDTO.class))
                .thenReturn(ClientMock.clientResponseDTO());

        ClientResponseDTO clientResult = clientService.create(client);
        assertThat(clientResult).isNotNull();
    }

    @Test
    void getById() {
        Client entity = ClientMock.client();

        when(clientRepository.findById(anyInt())).thenReturn(Optional.of(entity));
        when(conversionService.convert(entity, ClientResponseDTO.class))
                .thenReturn(ClientMock.clientResponseDTO());

        ClientResponseDTO clientResult = clientService.getById(1);
        assertThat(clientResult).isNotNull();
    }

    @Test
    void getById_NotFound() {

        when(clientRepository.findById(anyInt())).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class, () -> clientService.getById(1));
    }


    @Test
    void delete() {
        Client entity = ClientMock.client();

        when(clientRepository.findById(anyInt())).thenReturn(Optional.of(entity));
        doNothing().when(clientRepository).deleteById(anyInt());

        assertDoesNotThrow(() -> clientService.delete(1));
    }


    @Test
    void getAll() {
        when(clientRepository.findAll()).thenReturn(ClientMock.clientList());
        var result = clientService.getAll();

        assertNotNull(result);
        assertEquals(2, result.size());
    }


}
