package com.devsu.test.client.controller;

import com.devsu.test.client.dto.ClientDTO;
import com.devsu.test.client.dto.ClientResponseDTO;
import com.devsu.test.client.dto.ClientUpdateDTO;
import com.devsu.test.client.service.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller for managing clients.
 */
@RestController
@RequestMapping("/clientes")
@Tag(name = "Client", description = "Operations pertaining to clients in the Client Management System")
public class ClientController {

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    /**
     * Create a new client.
     *
     * @param clientDTO the client to create
     * @return the created client
     */
    @Operation(summary = "Create a new client")
    @PostMapping
    public ResponseEntity<ClientResponseDTO> createClient(@Valid @RequestBody ClientDTO clientDTO) {
        return new ResponseEntity(this.clientService.create(clientDTO), HttpStatus.CREATED);
    }

    /**
     * Update an existing client.
     *
     * @param clientDTO the client to update
     * @return the updated client
     */
    @Operation(summary = "Update an existing client")
    @PutMapping
    public ResponseEntity<ClientResponseDTO> updateClient(@Valid @RequestBody ClientUpdateDTO clientDTO) {
        return ResponseEntity.ok(this.clientService.update(clientDTO));
    }

    /**
     * Delete an existing client.
     *
     * @param id the id of the client to delete
     */
    @Operation(summary = "Update an existing client")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteClient(@PathVariable Integer id) {
        this.clientService.delete(id);
        return ResponseEntity.ok().build();
    }

    /**
     * Get all clients.
     *
     * @return list of all clients
     */
    @Operation(summary = "View a list of available clients")
    @GetMapping
    public ResponseEntity<List<ClientResponseDTO>> getAllClients() {
        return ResponseEntity.ok(this.clientService.getAll());
    }

    /**
     * Get a client by id.
     *
     * @param id the id of the client to retrieve
     * @return the client with the specified id
     */
    @Operation(summary = "Get a client by Id")
    @GetMapping("/{id}")
    public ResponseEntity<ClientResponseDTO> getClientById(@PathVariable Integer id) {
        return ResponseEntity.ok(this.clientService.getById(id));
    }
}
