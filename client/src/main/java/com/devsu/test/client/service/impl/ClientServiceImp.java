package com.devsu.test.client.service.impl;

import com.devsu.test.client.dto.ClientDTO;
import com.devsu.test.client.dto.ClientResponseDTO;
import com.devsu.test.client.dto.ClientUpdateDTO;
import com.devsu.test.client.exception.NotFoundException;
import com.devsu.test.client.model.Client;
import com.devsu.test.client.repository.ClientRepository;
import com.devsu.test.client.service.ClientService;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Transactional
@Service
public class ClientServiceImp implements ClientService {

    private final ClientRepository clientRepository;
    private final ConversionService conversionService;


    public ClientServiceImp(ClientRepository clientRepository, ConversionService conversionService) {
        this.clientRepository = clientRepository;
        this.conversionService = conversionService;
    }

    public ClientResponseDTO create(ClientDTO dto) {
        return save(dto);
    }

    public ClientResponseDTO update(ClientUpdateDTO dto) {
        this.findById(dto.getId());
        return save(dto);
    }

    public void delete(Integer id) {
        this.findById(id);
        this.clientRepository.deleteById(id);
    }


    public List<ClientResponseDTO> getAll() {
        return this.clientRepository.findAll().stream().map(c -> conversionService.convert(c, ClientResponseDTO.class)).toList();
    }

    public ClientResponseDTO getById(Integer id) {
        return conversionService.convert(this.findById(id), ClientResponseDTO.class);
    }

    private ClientResponseDTO save(ClientDTO dto) {
        Client newClient = this.clientRepository.save(Objects.requireNonNull(conversionService.convert(dto, Client.class)));
        return conversionService.convert(newClient, ClientResponseDTO.class);
    }

    private Client findById(Integer id) {
        return this.clientRepository.findById(id).orElseThrow(() -> new NotFoundException("client not found"));
    }

}
