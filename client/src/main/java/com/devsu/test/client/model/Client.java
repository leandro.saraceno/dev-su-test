package com.devsu.test.client.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "clients")
public class Client extends Person {

    private String clientId;
    private String password;
    private boolean status;

}
