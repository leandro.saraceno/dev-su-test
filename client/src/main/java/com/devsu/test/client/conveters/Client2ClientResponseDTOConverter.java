package com.devsu.test.client.conveters;

import com.devsu.test.client.dto.ClientResponseDTO;
import com.devsu.test.client.model.Client;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class Client2ClientResponseDTOConverter implements Converter<Client, ClientResponseDTO> {

    private final ModelMapper modelMapper;

    public Client2ClientResponseDTOConverter(@Lazy ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ClientResponseDTO convert(Client source) {
        return this.modelMapper.map(source, ClientResponseDTO.class);
    }

}
