package com.devsu.test.client.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends GenericException {

    private static final long serialVersionUID = 1L;

    public NotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}
