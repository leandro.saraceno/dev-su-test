package com.devsu.test.client.service;

import com.devsu.test.client.dto.ClientDTO;
import com.devsu.test.client.dto.ClientResponseDTO;
import com.devsu.test.client.dto.ClientUpdateDTO;

import java.util.List;

public interface ClientService {

    ClientResponseDTO create(ClientDTO dto);

    ClientResponseDTO update(ClientUpdateDTO dto);

    void delete(Integer id);

    List<ClientResponseDTO> getAll();

    ClientResponseDTO getById(Integer id);
}
