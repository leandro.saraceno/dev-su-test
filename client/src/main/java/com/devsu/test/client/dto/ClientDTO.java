package com.devsu.test.client.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ClientDTO {

    @NotEmpty
    private String name;
    @NotEmpty
    private String gender;
    @NotNull
    private Date birthDate;
    private String tel;
    private String address;
    @NotNull
    private String clientId;
    @NotNull
    private String password;
    @NotNull
    private boolean status;
}
