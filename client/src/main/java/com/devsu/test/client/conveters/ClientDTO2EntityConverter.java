package com.devsu.test.client.conveters;

import com.devsu.test.client.dto.ClientDTO;
import com.devsu.test.client.model.Client;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ClientDTO2EntityConverter implements Converter<ClientDTO, Client> {

    private final ModelMapper modelMapper;

    public ClientDTO2EntityConverter(@Lazy ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Client convert(ClientDTO source) {
        return this.modelMapper.map(source, Client.class);
    }

}
