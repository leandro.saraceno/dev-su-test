-- Insert into people table
INSERT INTO people (name, gender, birth_date, tel, address) VALUES
('Jose Lema', 'Male', '1980-01-01', '098254785', 'Otavalo sn y principal'),
('Marianela Montalvo', 'Female', '1985-05-15', '097548965', 'Amazonas y NNUU'),
('Juan Osorio', 'Male', '1990-09-20', '098874587', '13 junio y Equinoccial');

-- Insert into client table
INSERT INTO clients (id, client_id, password, status) VALUES
((SELECT id FROM people WHERE "name" = 'Jose Lema'), 'jose.lema', '1234', TRUE),
((SELECT id FROM people WHERE "name" = 'Marianela Montalvo'), 'marianela.montalvo', '5678', TRUE),
((SELECT id FROM people WHERE "name" = 'Juan Osorio'), 'juan.osorio', '1245', TRUE);

-- Insert into account_types table
INSERT INTO account_types (name) VALUES
('Ahorros'),
('Corriente');

-- Insert into accounts table
INSERT INTO accounts (id, initial_balance, client_id, status, type_id) VALUES
(478758, 2000, (SELECT id FROM people WHERE "name" = 'Jose Lema'), TRUE, (SELECT id FROM account_types WHERE name = 'Ahorros')),
(225487, 100, (SELECT id FROM people WHERE "name" = 'Marianela Montalvo'), TRUE, (SELECT id FROM account_types WHERE name = 'Corriente')),
(495878, 0, (SELECT id FROM people WHERE "name" = 'Juan Osorio'), TRUE, (SELECT id FROM account_types WHERE name = 'Ahorros')),
(496825, 540, (SELECT id FROM people WHERE "name" = 'Marianela Montalvo'), TRUE, (SELECT id FROM account_types WHERE name = 'Ahorros'));

INSERT INTO public.transaction_types
(id, "name")
VALUES(1, 'Deposito');
INSERT INTO public.transaction_types
(id, "name")
VALUES(2, 'Retiro');


INSERT INTO public.transactions
(id, "date", type_id, value, balance, account_id)
VALUES(1, 'now()', 2, 575.0, 1425.0, 478758);

INSERT INTO public.transactions
(id, "date", type_id, value, balance, account_id)
VALUES(2, 'now()', 1, 600.0, 700.0, 225487);

INSERT INTO public.transactions
(id, "date", type_id, value, balance, account_id)
VALUES(3, 'now()', 1, 150, 150.0, 495878);

INSERT INTO public.transactions
(id, "date", type_id, value, balance, account_id)
VALUES(4, 'now()', 2, -540.0, 0, 496825);