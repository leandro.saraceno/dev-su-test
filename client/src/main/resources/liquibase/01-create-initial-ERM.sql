-- Create table for People
CREATE TABLE PEOPLE (
                        id SERIAL PRIMARY KEY,
                        "name" VARCHAR(255) NOT NULL,
                        gender VARCHAR(10) NOT NULL,
                        birth_date DATE NOT NULL,
                        tel VARCHAR(20),
                        address VARCHAR(255)
);

-- Create table for Client
CREATE TABLE clients (
                         id INTEGER PRIMARY KEY,
                         client_id VARCHAR(50) NOT NULL,
                         password VARCHAR(255) NOT NULL,
                         status BOOLEAN NOT NULL,
                         CONSTRAINT fk_clients_people FOREIGN KEY (id) REFERENCES people (id)
);


-- Create table for account_types
CREATE TABLE account_types (
                               id SERIAL PRIMARY KEY,
                               "name" VARCHAR(255) NOT NULL
);

-- Create table for accounts
CREATE TABLE accounts (
                          id SERIAL PRIMARY KEY,
                          initial_balance DOUBLE PRECISION NOT NULL,
                          client_id INTEGER NOT NULL,
                          status BOOLEAN NOT NULL,
                          type_id INTEGER NOT NULL,
                          CONSTRAINT fk_account_type FOREIGN KEY (type_id) REFERENCES account_types (id),
                          CONSTRAINT fk_clients FOREIGN KEY (client_id) REFERENCES clients (id)
);

-- Create table for transaction_types
CREATE TABLE transaction_types (
                                   id SERIAL PRIMARY KEY,
                                   "name" VARCHAR(255) NOT NULL
);

-- Create table for transactions
CREATE TABLE transactions (
                              id SERIAL PRIMARY KEY,
                              "date" DATE NOT NULL,
                              type_id INTEGER NOT NULL,
                              value DOUBLE PRECISION NOT NULL,
                              balance DOUBLE PRECISION NOT NULL,
                              account_id INTEGER NOT NULL,
                              CONSTRAINT fk_transaction_type FOREIGN KEY (type_id) REFERENCES transaction_types (id),
                              CONSTRAINT fk_transaction_acccount FOREIGN KEY (account_id) REFERENCES accounts (id)
);


CREATE OR REPLACE VIEW public.account_status_report_view
AS SELECT row_number() OVER () AS id,
          c.id AS client_id,
          a.id AS account_id,
          act.name AS type,
          COALESCE(sum(t.value), 0::double precision) AS current_balance
   FROM accounts a
            JOIN clients c ON c.id = a.client_id
            JOIN account_types act ON act.id = a.type_id
            LEFT JOIN transactions t ON a.id = t.account_id
   GROUP BY a.id, c.id, act.name;